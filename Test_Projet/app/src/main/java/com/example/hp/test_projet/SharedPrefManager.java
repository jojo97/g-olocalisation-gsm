package com.example.hp.test_projet;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;
    public static final String Shared_Pref_Name = "mysharedpref12";
    private static final String Key_Username = "username";
    private static final String Key_Id = "userid";
    private static final String Key_Email = "useremail";
    private static final String Key_Latit = "userLatit";
    private static final String Key_Longit = "userLongit";
    private static final String Key_Secours = "userSecours";
    private static final String Key_PSW = "userpassword";

    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }


    public boolean userLogin(int id, String username, String password, String email, String Longit, String Latit, String secours){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        editor.putInt(Key_Id, id);
        editor.putString(Key_Username, username);
        editor.putString(Key_PSW, password);
        editor.putString(Key_Email, email);
        editor.putString(Key_Longit, Longit);
        editor.putString(Key_Latit, Latit);
        editor.putString(Key_Secours, secours);
        editor.apply();
        return true;
    }

    public boolean IsLogIn(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        if(sh.getString(Key_Username, null) != null){
            return true;

        }else{
            return false;
        }
    }

    public boolean IsLogOut(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    public boolean clean(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    public String getUsername(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        return sh.getString(Key_Username, null);
    }

    public String getKey_Longit(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        return sh.getString(Key_Longit, null);
    }

    public String getKey_Latit(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        return sh.getString(Key_Latit, null);
    }

    public String getKey_Secours(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        return sh.getString(Key_Secours, null);
    }

    public String getKey_PSW(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        return sh.getString(Key_PSW, null);
    }

    public int getId(){
        SharedPreferences sh = mCtx.getSharedPreferences(Shared_Pref_Name, Context.MODE_PRIVATE);
        return sh.getInt(Key_Id, 0);
    }
}