package com.example.hp.test_projet;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Recherche extends AppCompatActivity implements View.OnClickListener {
    private String lat,lon;
    private Button BtnRec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recherche);
        BtnRec = (Button) findViewById(R.id.BtnRec);

        lat = SharedPrefManager.getInstance(this).getKey_Latit();
        lon = SharedPrefManager.getInstance(this).getKey_Longit();

        BtnRec.setOnClickListener(this);

        mapview2();

    }

    public void mapview2() {
        String Position = null;
        String stringLoc = "geo:" + lat + "," + lon;

        Toast.makeText(Recherche.this, stringLoc, Toast.LENGTH_LONG).show();

        /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(stringLoc + "(" + Position + ")"));
        startActivity(intent);*/

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q="+lat+","+lon+" (" + Position + ")"));
        startActivity(intent);
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(), Localiser_Personne.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v == BtnRec){
            startActivity(new Intent(getApplicationContext(), Principale.class));
            finish();
        }
    }
}
