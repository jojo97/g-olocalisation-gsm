<?php

require_once '../includes/DbOperations.php';
$response = array();

if($_SERVER['REQUEST_METHOD']=='POST'){

	if(($_POST['username'])!=""){
		$db = new DbOperations();
		$username = $_POST['username'];
		$user = $db->getUserByUsername($username);
		$response['error'] = false;
		$response['id'] = $user['id']; 
		$response['username'] = $user['username'];
		$response['password'] = $user['password'];
		$response['email'] = $user['email'];
		$response['longitude'] = $user['Longit'];
		$response['latitude'] = $user['Latit'];
		$response['secours'] = $user['secours'];
		$response['message'] = "Connexion réussie";	

	}else{
		$response['error'] = true;
	    $response['message'] = "Il faut remplir tout les champs";
	}
}
echo json_encode($response);

?>