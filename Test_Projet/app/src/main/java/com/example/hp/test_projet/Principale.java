package com.example.hp.test_projet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Principale extends AppCompatActivity implements View.OnClickListener {
    private Button btview, search, send, option, btdeco;
    private String lon, lat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);

        search = (Button) findViewById(R.id.search);
        btview = (Button) findViewById(R.id.viewmap);
        send = (Button) findViewById(R.id.send);
        option = (Button) findViewById(R.id.option);
        btdeco = (Button) findViewById(R.id.button2);

        btview.setOnClickListener(this);
        search.setOnClickListener(this);
        send.setOnClickListener(this);
        option.setOnClickListener(this);
        btdeco.setOnClickListener(this);

        lon = SharedPrefManager.getInstance(this).getKey_Longit();
        lat = SharedPrefManager.getInstance(this).getKey_Latit();

    }

    public void btview() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
        String networkOperator = telephonyManager.getNetworkOperator();

        final String username = SharedPrefManager.getInstance(this).getUsername();
        final String mcc = networkOperator.substring(0, 3);
        final String mnc = networkOperator.substring(3);
        final int cid = cellLocation.getCid();
        final int lac = cellLocation.getLac();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constant.Url_Up,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String reponse) {
                        try {
                            JSONObject obj = new JSONObject(reponse);
                            if (!obj.getBoolean("error")) {
                                mapview1();
                            } else {
                                Toast.makeText(
                                        getApplicationContext(),
                                        obj.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        getApplicationContext(),
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cid", String.valueOf(cid));
                params.put("lac", String.valueOf(lac));
                params.put("mnc", mnc);
                params.put("mcc", mcc);
                params.put("username", username);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

    }

    public void mapview1() {
        String Position = null;
        String stringLoc = "geo:" + lat + "," + lon;

        Toast.makeText(Principale.this, stringLoc, Toast.LENGTH_LONG).show();

        /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(stringLoc + "(" + Position + ")"));
        startActivity(intent);*/

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q="+lat+","+lon+" (" + Position + ")"));
        startActivity(intent);
    }

    @Override
    public void onBackPressed()
    {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Deconnexion");
        progressDialog.show();
        if (SharedPrefManager.getInstance(this).IsLogOut()) {
            finish();
            startActivity(new Intent(getApplicationContext(), Connexion.class));
        }
    }

    public boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            return true;
        }else{
            return false;
        }
    }



    @Override
    public void onClick(View v) {
        ProgressDialog progressDialog = new ProgressDialog(this);

        if (v == btview) {
                if(isConnected()){
                        progressDialog.setMessage("Update en cours");
                        btview();
                }else{
                    Toast.makeText(
                            getApplicationContext(),"Vous n'etes pas connecté a internet",
                            Toast.LENGTH_LONG
                    ).show();
                }
        }

        if(v == search){
            SharedPrefManager.getInstance(this).clean();
            startActivity(new Intent(getApplicationContext(), Localiser_Personne.class));
            finish();
        }

        if (v == send){
            if(isConnected()){
                startActivity(new Intent(getApplicationContext(), Envoyer_Message.class));
                finish();
            }else{
                Toast.makeText(
                        getApplicationContext(),"Vous n'etes pas connecté a internet",
                        Toast.LENGTH_LONG
                ).show();
            }
        }

        if(v == option){
            if(isConnected()){
                startActivity(new Intent(getApplicationContext(), Plus_doption.class));
                finish();
            }else{
                Toast.makeText(
                        getApplicationContext(),"Vous n'etes pas connecté a internet",
                        Toast.LENGTH_LONG
                ).show();
            }
        }

        if(v == btdeco){
            progressDialog.setMessage("Deconnexion");
            progressDialog.show();
            if (SharedPrefManager.getInstance(this).IsLogOut()) {
                finish();
                startActivity(new Intent(getApplicationContext(), Connexion.class));
            }
        }
    }
}
