<?php
require_once '../includes/DbOperations.php';
$response = array();
$db = new DbOperations();
$username = $_POST['username'];
$password = $_POST['password'];

if($_SERVER['REQUEST_METHOD']=='POST'){
    
    if($username !="" and $password !=""){
        if(strlen($_POST['password']) >= 5){
        $user = $db->getUserByUsername($username);
            $response['error'] = false;
            $response['message'] = "Mot de passe modifié";
            $db->UpdateUserPassword($password,$username);
        }else{
            $response['error'] = true;
            $response['message'] = "Le mot de passe doit contenir au moins 5 caractères";
        }
    }
    else{
        $response['error'] = true;
        $response['message'] = "Il faut remplir tout les champs";
    }
}else{
        $response['error'] = true;
        $response['message'] = "Requete invalide";
}
echo json_encode($response);
?>