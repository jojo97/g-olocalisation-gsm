package com.example.hp.test_projet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Inscription extends AppCompatActivity implements View.OnClickListener {

    private EditText NomU, Email, Mdp, Tel, Secours;
    private Button Btn;
    private TextView tvConnexion;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);


        NomU = (EditText) findViewById(R.id.NomU);
        Email = (EditText) findViewById(R.id.Email);
        Mdp = (EditText) findViewById(R.id.Mdp);
        tvConnexion = (TextView) findViewById(R.id.tvConnexion);
        Tel = (EditText) findViewById(R.id.Tel) ;
        Secours = (EditText) findViewById(R.id.secours);
        Btn = (Button) findViewById(R.id.Btn);
        tvConnexion.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);

        Btn.setOnClickListener(this);
    }




    private void registerUser(){
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
        String networkOperator = telephonyManager.getNetworkOperator();
        String mcc = networkOperator.substring(0, 3);
        String mnc = networkOperator.substring(3);
        int cid = cellLocation.getCid();
        int lac = cellLocation.getLac();
        final String username = NomU.getText().toString().trim();
        final String email = Email.getText().toString().trim();
        final String password = Mdp.getText().toString().trim();
        final String cellphone = Tel.getText().toString().trim();
        final String secours = Secours.getText().toString().trim();
        final String Textmcc = mcc;
        final String Textmnc = mnc;
        final String Textcid = String.valueOf(cid);
        final String Textlac = String.valueOf(lac);


        progressDialog.setMessage("Enregistrement de l'utilisateur");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constant.Url_Register,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(!jsonObject.getBoolean("error")) {
                                Toast.makeText(
                                        getApplicationContext(),
                                        jsonObject.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                                startActivity(new Intent(getApplicationContext(), Connexion.class));
                                finish();
                            }else{
                                Toast.makeText(
                                        getApplicationContext(),
                                        jsonObject.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                Toast.makeText(
                        getApplicationContext(),
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                params.put("email", email);
                params.put("cellphone", cellphone );
                params.put("cid", Textcid);
                params.put("lac", Textlac);
                params.put("mnc", Textmnc);
                params.put("mcc", Textmcc);
                params.put("secours", secours);
                return params;

            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    public boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(), Connexion.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v == Btn) {
            if (isConnected()) {
                registerUser();
            } else {
                Toast.makeText(
                        getApplicationContext(), "Vous n'etes pas connecté a internet",
                        Toast.LENGTH_LONG
                ).show();
            }
        }
        if(v == tvConnexion){
            startActivity(new Intent(this, Connexion.class));}

    }
}