<?php 

//Classe pour la connexion
class DbConnection{
	private $con;
	
	//Constructeur
	function __construct(){
	}

	//Fonction de connexion a la base de donnée
	function connect(){
		include_once dirname(__FILE__).'/Constants.php';
		$this->con = new mysqli(DB_HOST, DB_USER, DB_PSW, DB_NAME);

		if(mysqli_connect_errno()){
			echo 'Erreur de connexion bd'.mysqli_connect_errno();
		}


		return $this->con;
	}


}
?>