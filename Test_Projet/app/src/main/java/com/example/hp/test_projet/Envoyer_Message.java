package com.example.hp.test_projet;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.widget.Toast;

public class Envoyer_Message extends AppCompatActivity {

    private String Num,lon,lat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envoyer__message);

        Num = (SharedPrefManager.getInstance(this).getKey_Secours());

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        1);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        1);
            }
        }

        try{
            lon = SharedPrefManager.getInstance(this).getKey_Longit();
            lat = SharedPrefManager.getInstance(this).getKey_Latit();
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(Num, null, "Je suis en danger !! Localises moi aux coordonnées suivantes : longitude = "+
                    lon+" latitude = "+lat, null, null);
            Toast.makeText(this, "Message envoyé", Toast.LENGTH_SHORT).show();
            finish();
            startActivity(new Intent(getApplicationContext(), Principale.class));
        }
        catch (Exception e){
            Toast.makeText(this, "Echec d'envoie", Toast.LENGTH_SHORT).show();
            finish();
            startActivity(new Intent(getApplicationContext(), Principale.class));
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(ContextCompat.checkSelfPermission(this,
                            Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(this, "Autorisé", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "No", Toast.LENGTH_LONG).show();

                }
            }
        }

    }
}
