package com.example.hp.test_projet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class actu_mdp extends AppCompatActivity implements View.OnClickListener {
    private EditText new_mdp,ancien_mdp;
    private Button bt_modif;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actu_mdp);

        bt_modif = (Button) findViewById(R.id.Modifier);
        new_mdp = (EditText) findViewById(R.id.new_mdp);
        ancien_mdp = (EditText) findViewById(R.id.ancien_mdp);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Modification en cours");

        bt_modif.setOnClickListener(this);

    }

    private void UpdatPsw() {
        final String username = SharedPrefManager.getInstance(getApplicationContext()).getUsername();
        final String NewMdp = new_mdp.getText().toString().trim();
        final String x = ancien_mdp.getText().toString().trim();
        final String AncienPsw1 = SharedPrefManager.getInstance(getApplicationContext()).getKey_PSW();
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constant.Url_UpdatePsw,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (!obj.getBoolean("error")){
                                if (!AncienPsw1.equals(NewMdp)){
                                    if(x.equals(AncienPsw1)){
                                        SharedPrefManager.getInstance(getApplicationContext());
                                        Toast.makeText(
                                                getApplicationContext(),
                                                obj.getString("message"),
                                                Toast.LENGTH_LONG
                                        ).show();
                                        startActivity(new Intent(getApplicationContext(), Plus_doption.class));
                                        finish();

                                    }else{
                                        Toast.makeText(
                                                getApplicationContext(), "Vérifiez votre ancien mot de passe",
                                                Toast.LENGTH_LONG
                                        ).show();
                                    }
                                } else {
                                    Toast.makeText(
                                            getApplicationContext(),"Les mots de passe doivent etre différent",
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }else{
                                Toast.makeText(
                                        getApplicationContext(),
                                        obj.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        getApplicationContext(),
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("password", NewMdp);
                params.put("username", username);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

    }

    public boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            return true;
        }else{
            return false;
        }
    }



    @Override
    public void onClick(View v) {
        String x = SharedPrefManager.getInstance(getApplicationContext()).getKey_PSW();
        if (v == bt_modif) {
            if (isConnected()) {
                    UpdatPsw();
            } else {
                Toast.makeText(
                        getApplicationContext(), "Vous n'etes pas connecté a internet",
                        Toast.LENGTH_LONG
                ).show();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(), Plus_doption.class));
        finish();
    }
}
