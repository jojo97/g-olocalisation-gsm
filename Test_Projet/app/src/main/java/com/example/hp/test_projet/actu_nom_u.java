package com.example.hp.test_projet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class actu_nom_u extends AppCompatActivity implements View.OnClickListener {

    private EditText new_nomU,ancien_nomU;
    private Button modifier;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actu_nom_u);

        new_nomU = (EditText) findViewById(R.id.new_nomU);
        modifier = (Button) findViewById(R.id.BtnValider);
        ancien_nomU = (EditText) findViewById(R.id.ancien_nomU);

        modifier.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Modification en cours");
    }



    @Override
    public void onBackPressed()
    {
            startActivity(new Intent(getApplicationContext(), Plus_doption.class));
            finish();
    }

    private void UpdatNomU() {
        final int id = SharedPrefManager.getInstance(getApplicationContext()).getId();
        final String new_username = new_nomU.getText().toString().trim();
        final String x = ancien_nomU.getText().toString().trim();
        final String ancien_nom_U = SharedPrefManager.getInstance(getApplicationContext()).getUsername();
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constant.Url_UpdateNomU,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                if (!new_username.equals(ancien_nom_U)){
                                    if(x.equals(ancien_nom_U)){
                                        SharedPrefManager.getInstance(getApplicationContext());
                                        Toast.makeText(
                                                getApplicationContext(),
                                                obj.getString("message"),
                                                Toast.LENGTH_LONG
                                        ).show();
                                        startActivity(new Intent(getApplicationContext(), Plus_doption.class));
                                        finish();
                                    }else{
                                        Toast.makeText(
                                                getApplicationContext(), "Verifiez votre actuel nom",
                                                Toast.LENGTH_LONG
                                        ).show();
                                    }
                                } else {
                                    Toast.makeText(
                                            getApplicationContext(),"Les nom d'utilisateur doivent etre différent",
                                            Toast.LENGTH_LONG
                                    ).show();
                                }

                            }else{
                                Toast.makeText(
                                        getApplicationContext(),obj.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        getApplicationContext(),
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", new_username);
                params.put("id", String.valueOf(id));
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

    }

    public boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        if(v == modifier){
            if (isConnected()) {
                    UpdatNomU();
            } else {
                Toast.makeText(
                        getApplicationContext(), "Vous n'etes pas connecté a internet",
                        Toast.LENGTH_LONG
                ).show();
            }
        }
        }
    }

