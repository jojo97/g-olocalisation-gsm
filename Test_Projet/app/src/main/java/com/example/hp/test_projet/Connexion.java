package com.example.hp.test_projet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Connexion extends AppCompatActivity implements View.OnClickListener {

    private EditText NomU2, Mdp2;
    private Button BtnValider;
    private TextView tvInscription;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        NomU2 = (EditText) findViewById(R.id.NomU2);
        Mdp2 = (EditText) findViewById(R.id.Mdp2);
        BtnValider = (Button) findViewById(R.id.BtnValider);
        tvInscription = (TextView) findViewById(R.id.tvInscription);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Patientez un instant...");
        BtnValider.setOnClickListener(this);
        tvInscription.setOnClickListener(this);
    }
    private void userLogin() {
        final String username = NomU2.getText().toString().trim();
        final String password = Mdp2.getText().toString().trim();
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
        String networkOperator = telephonyManager.getNetworkOperator();

        final String mcc = networkOperator.substring(0, 3);
        final String mnc = networkOperator.substring(3);
        final int cid = cellLocation.getCid();
        final int lac = cellLocation.getLac();
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constant.Url_Login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (!obj.getBoolean("error")) {
                                SharedPrefManager.getInstance(getApplicationContext())
                                        .userLogin(
                                                obj.getInt("id"),
                                                obj.getString("username"),
                                                obj.getString("password"),
                                                obj.getString("email"),
                                                obj.getString("longitude"),
                                                obj.getString("latitude"),
                                                obj.getString("secours"));
                                SharedPrefManager.getInstance(getApplicationContext());
                                Toast.makeText(
                                        getApplicationContext(),
                                        obj.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                                startActivity(new Intent(getApplicationContext(), Principale.class));
                                finish();

                            } else {
                                Toast.makeText(
                                        getApplicationContext(),
                                        obj.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(
                        getApplicationContext(),
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                params.put("cid", String.valueOf(cid));
                params.put("lac", String.valueOf(lac));
                params.put("mnc", mnc);
                params.put("mcc", mcc);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

    }

    public boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            return true;
        }else{
            return false;
        }
    }


    @Override
    public void onClick(View v) {
        if (v == BtnValider) {
            if(isConnected()){
                userLogin();
            }else{
                Toast.makeText(
                        getApplicationContext(),"Vous n'etes pas connecté a internet",
                        Toast.LENGTH_LONG
                ).show();
            }
        }
        if (v == tvInscription)
            startActivity(new Intent(this, Inscription.class));
    }
}

