<?php

require_once '../includes/DbOperations.php';
$response = array();
$username = $_POST['username'];
$db = new DbOperations();

if($_SERVER['REQUEST_METHOD']=='POST'){

	if($username !=""){
		$result = $db->IsUserExist3($username);

	    if($result == 0){
	    $db->DeleteUser($username);
		$response['error'] = false;
	    $response['message'] = "Utilisateur supprimé";
	}else{
		$response['error'] = true;
	    $response['message'] = "L'utilisateur n'existe pas";
	}

}else{
		$response['error'] = true;
	    $response['message'] = "Il faut remplir le champ";
	}
}else{
		$response['error'] = true;
	    $response['message'] = "Requete invalide";
	}
echo json_encode($response);
?>