<?php
require_once '../includes/DbOperations.php';
$response = array();
$db = new DbOperations();
$username = $_POST['username'];
$id = $_POST['id'];

if($_SERVER['REQUEST_METHOD']=='POST'){
	
	if($username !="" and $id !=""){
		$user = $db->getUserByUsername($username);
		if($user['username'] == null){
			 if(ctype_alpha($_POST['username'])){
               if(strlen($_POST['username']) >= 4){
			$db->UpdateUsername($username,$id);
			$response['error'] = false;
	        $response['message'] = "Nom d'utilisateur modifié";
               }else{
            $response['error'] = true;
            $response['message'] = "Le nom d'utilisateur doit contenir au moins 4 caractères";
               }
           }else{
            $response['error'] = true;
            $response['message'] = "Le nom d'utilisateur doit contenir des lettres";
           }
		}else{
			$response['error'] = true;
	        $response['message'] = "Le nom d'utilisateur existe deja";
		}
	}
	else{
		$response['error'] = true;
	    $response['message'] = "Il faut remplir tout les champs";
	}
}else{
		$response['error'] = true;
	    $response['message'] = "Requete invalide";
}
echo json_encode($response);
?>