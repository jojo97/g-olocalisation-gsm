<?php

//Classe pour les operations dans la base
class DbOperations{

	private $con;

	//Constructeur
	function __construct(){
		require_once dirname(__FILE__).'/DbConnection.php';
		$db = new DbConnection();
		$this->con = $db->connect();
	}

	//CRUD -> C -> CREATE
	public function CreateUser($username, $password, $email, $cid, $lac, $mnc, $mcc, $longitude, $latitude, $cellphone, $secours){
		if($this->IsUserExist($username,$email)){
			return 0;
		}
		else{
			//Statement
			$stmt = $this->con->prepare("INSERT INTO `geolocalisation`.`compte` (`id`, `username`, `password`, `email`, `cid`, `lac`, `mnc`, `mcc`, `Longit`, `Latit`, `cellphone`, `secours`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ".$longitude.", ".$latitude.", ?, ?);");
		    $stmt->bind_param("sssssssss",$username,$password,$email,$cid,$lac,$mnc,$mcc,$cellphone,$secours);
		    //Execution du Statement
		    if($stmt->execute()){
		    	return 1;
		    }else{
		    	return 2;
		    }
		}
	}


    public function UpdateUser($cid, $lac, $mnc, $mcc, $Longit, $Latit, $username){
    	$stmt = $this->con->prepare("UPDATE `geolocalisation`.`compte` SET `cid` = ?, `lac` = ?, `mnc` = ?, `mcc` = ?, `Longit` = ".$Longit.", `Latit` = ".$Latit."  WHERE `compte`.`username` = ?;");
		$stmt->bind_param("sssss",$cid,$lac,$mnc,$mcc,$username);
		$stmt->execute();
	}

	public function userLogin($username, $password){
		$stmt = $this->con->prepare("SELECT id FROM compte WHERE username = ? AND password = ?");
		$stmt->bind_param("ss",$username,$password);
		$stmt->execute();
		$stmt->store_result();
		return $stmt->num_rows > 0;
	}

	public function getUserByUsername($username){
		$stmt = $this->con->prepare("SELECT * FROM compte WHERE username = ?");
		$stmt->bind_param("s",$username);
		$stmt->execute();
		return $stmt->get_result()->fetch_assoc();
	}

	private function IsUserExist($username, $email){
		$stmt = $this->con->prepare("SELECT id FROM compte WHERE username = ? OR email = ?");
		$stmt->bind_param("ss", $username, $email);
		$stmt->execute();
		$stmt->store_result();
		return $stmt->num_rows > 0;
	}

	public function getUserByCellphone($cellphone){
		$stmt = $this->con->prepare("SELECT * FROM compte WHERE cellphone = ?");
		$stmt->bind_param("s",$cellphone);
		$stmt->execute();
		return $stmt->get_result()->fetch_assoc();
	}

	public function getUserId($username){
		$stmt = $this->con->prepare("SELECT id FROM compte WHERE username = ?");
		$stmt->bind_param("s",$username);
		$stmt->execute();
		return $stmt->get_result()->fetch_assoc();
	}

	public function getUsernamePasw($id){
		$stmt = $this->con->prepare("SELECT username,password FROM compte WHERE id = ?");
		$stmt->bind_param("s",$id);
		$stmt->execute();
		return $stmt->get_result()->fetch_assoc();
	}

	public function IsUserNameExist($username){
		$stmt = $this->con->prepare("SELECT id FROM compte WHERE username = ?");
		$stmt->bind_param("s",$username);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows > 0){
			return 0;
		}
		else{
			return 1;
		}
	}

	public function UpdateUserEmailByAdmin($email,$username){
		$stmt = $this->con->prepare("UPDATE `geolocalisation`.`compte` SET `email` = ? WHERE `compte`.`username` = ?;");
		$stmt->bind_param("ss",$email,$username);
		$stmt->execute();
	}

	public function IsUserExist2($username){
		$stmt = $this->con->prepare("SELECT id FROM compte WHERE username = ?");
		$stmt->bind_param("s",$username);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows > 0){
			return 1;
		}else{
			return 2;
		}
	}

	public function UpdateUsername($username,$id){
		$stmt = $this->con->prepare("UPDATE `geolocalisation`.`compte` SET `username` = ? WHERE `compte`.`id` = ?;");
		$stmt->bind_param("ss",$username,$id);
		$stmt->execute();
	}

	public function UpdateUserPassword($password,$username){
		$stmt = $this->con->prepare("UPDATE `geolocalisation`.`compte` SET `password` = ?  WHERE `compte`.`username` = ?;");
		$stmt->bind_param("ss",$password,$username);
		$stmt->execute();
	}

	public function DeleteUser($username){
		$stmt = $this->con->prepare("DELETE FROM `geolocalisation`.`compte` WHERE `compte`.`username` = ?;");
		$stmt->bind_param("s",$username);
		$stmt->execute();
	}

	public function IsUserExist3($username){
		$stmt = $this->con->prepare("SELECT id FROM compte WHERE username = ?");
		$stmt->bind_param("s", $username);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows > 0){
			return 0;
		}else{
			return 1;
		}
	}


	private function IsAdminExist($username){
		$stmt = $this->con->prepare("SELECT id FROM administrateur WHERE username = ?");
		$stmt->bind_param("s",$username);
		$stmt->execute();
		$stmt->store_result();
		return $stmt->num_rows > 0;
	}

	public function CreateAdmin($username,$password){
		if($this->IsAdminExist($username)){
			return 0;
		}
		else{
			//Statement
			$stmt = $this->con->prepare("INSERT INTO `geolocalisation`.`administrateur` (`username`, `password`) VALUES (?, ?);");
		    $stmt->bind_param("ss",$username,$password);
		    //Execution du Statement
		    if($stmt->execute()){
		    	return 1;
		    }else{
		    	return 2;
		    }
		}
	}

	public function getAdminByUsername($username){
		$stmt = $this->con->prepare("SELECT * FROM administrateur WHERE username = ?");
		$stmt->bind_param("s",$username);
		$stmt->execute();
		return $stmt->get_result()->fetch_assoc();
	}

	public function AdminLogin($username, $password){
		$stmt = $this->con->prepare("SELECT id FROM administrateur WHERE username = ? AND password = ?");
		$stmt->bind_param("ss",$username,$password);
		$stmt->execute();
		$stmt->store_result();
		return $stmt->num_rows > 0;
	}

	public function AllUser($param){
		$stmt = $this->con->prepare("SELECT ? FROM compte");
		$stmt->bind_param("s",$param);
		$stmt->execute();
		return $stmt->get_result()->fetch_assoc();
	}

}
?>
