package com.example.hp.test_projet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Plus_doption extends AppCompatActivity implements View.OnClickListener {
    private Button actu_nom_u,actu_mdp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plus_doption);
        actu_nom_u = (Button) findViewById(R.id.actu_nom_u);
        actu_mdp = (Button) findViewById(R.id.actu_mdp);

        actu_nom_u.setOnClickListener(this);
        actu_mdp.setOnClickListener(this);
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(), Principale.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v == actu_nom_u){
            startActivity(new Intent(getApplicationContext(), actu_nom_u.class));
            finish();
        }

        if(v == actu_mdp){
            startActivity(new Intent(getApplicationContext(), actu_mdp.class));
            finish();
        }
    }
}
