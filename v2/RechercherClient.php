<?php

require_once '../includes/DbOperations.php';
$response = array();

$username = $_POST['username'];
$db = new DbOperations();
if($_SERVER['REQUEST_METHOD']=='POST'){

	if($username !=""){
		$result = $db->IsUserExist2($username);
		if($result == 1){
		$user = $db->getUserByUsername($username);
		$response['id'] = $user['id'];
		$response['username'] = $user['username'];
		$response['password'] = $user['password'];
		$response['email'] = $user['email'];
		$response['longitude'] = $user['Longit'];
	    $response['latitude'] = $user['Latit'];
		$response['cellphone'] = $user['cellphone'];
		$response['secours'] = $user['secours'];
		$response['error'] = false;
	    $response['message'] = "Affichage en cours";
		}else{
		$response['error'] = true;
	    $response['message'] = "L'utilisateur n'existe pas";
		}
	}else{
		$response['error'] = true;
	    $response['message'] = "Il faut remplir tout les champs";
	}
}else{
		$response['error'] = true;
	    $response['message'] = "Requete invalide";
	}
echo json_encode($response);
?>